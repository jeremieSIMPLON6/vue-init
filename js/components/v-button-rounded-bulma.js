Vue.component('v-button-rounded-bulma', {
    props: ['title', 'url'],
    template: '<a class="button is-outlined is-rounded" \
                :href="url">{{title}}</a>'
})