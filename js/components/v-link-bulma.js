Vue.component('v-link-bulma', {
    props: ['title', 'url'],
    template: '<a class="button is-text is-my-link" \
                :href="url">{{title}}</a>'
})