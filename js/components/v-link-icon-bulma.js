Vue.component('v-link-icon-bulma', {
    props: ['url', 'icon'],
    template: '<a class="button is-text is-my-icon" \
                :href="url"><i :class="icon"></i></a>'
})